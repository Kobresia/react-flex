export function changeSize(size) {
  return dispatch => {
    dispatch({
      type: "CHANGE_SIZE",
      size: size
    });
  };
}
