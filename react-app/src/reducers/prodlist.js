const initialState = {
  cat1Tit: "Heritage Trench Coats",
  cat2Tit: "Single Breasted Trench Coats",
  cat1: [
    {
      src: "img/recommend-Emroided-Hooded.jpg",
      price: "27 000 руб.",
      title: "Emroided Hooded 2 Lines"
    },
    {
      src: "img/recommend-Relaxed-Fit-Stretch-Jeans.jpg",
      price: "22 500 руб.",
      title: "Relaxed Fit Stretch Jeans"
    },
    {
      src: "img/recommend-Leather-and-House-Check.jpg",
      price: "120 000 руб.",
      title: "Leather and House Check"
    },
    {
      src: "img/recommend-Leather-Wingtip-Check.jpg",
      price: "46 000 руб",
      title: "Leather Wingtip Check"
    }
  ],
  cat2: [
    {
      src: "img/recommend-Emroided-Hooded.jpg",
      price: "27 000 руб.",
      title: "Emroided Hooded 2 Lines"
    },
    {
      src: "img/recommend-Relaxed-Fit-Stretch-Jeans.jpg",
      price: "22 500 руб.",
      title: "Relaxed Fit Stretch Jeans"
    },
    {
      src: "img/recommend-Leather-and-House-Check.jpg",
      price: "120 000 руб.",
      title: "Leather and House Check"
    },
    {
      src: "img/recommend-Leather-Wingtip-Check.jpg",
      price: "46 000 руб",
      title: "Leather Wingtip Check"
    }
  ]
};

export default function prodlist(state = initialState, action) {
  return state;
}
