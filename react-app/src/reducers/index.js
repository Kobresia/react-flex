import { combineReducers } from "redux";
import header from "./header";
import footer from "./footer";
import recommend from "./recommend";
import prodlist from "./prodlist";
import prodDesc from "./prodDesc";
import card from "./card";
import { routerReducer } from "react-router-redux";
export default combineReducers({
  routing: routerReducer,
  header,
  footer,
  prodDesc,
  recommend,
  prodlist,
  card
});
