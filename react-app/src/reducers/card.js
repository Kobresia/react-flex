const initialState = {
  size: "S"
};

export default function card(state = initialState, action) {
  switch (action.type) {
    case "CHANGE_SIZE":
      return { ...state, size: action.size };
    default:
      return state;
  }
}
