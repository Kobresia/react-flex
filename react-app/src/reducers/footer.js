const initialState = {
  leftmenu: [
    "Customer service",
    "Contact Us",
    "Payment",
    "Shipping",
    "Returns",
    "Faqs",
    "Live Chat",
    "The Burberry App",
    "Store Locator"
  ],
  middlemenu: [
    "Our company",
    "Our History",
    "Burberry Group Plc",
    "Careers",
    "Corporate Responsibility",
    "Site Map"
  ],
  rightmenu: [
    "Legal & Cookies",
    "Terms & Conditions",
    "Privacy Policy",
    "Cookie Policy",
    "Accessibility Statement",
    "Japan Only - SCTL indications"
  ],
  bottommenu: {
    ship: ["Shipping country", "United Kingdom (£)"],
    lang: ["Language", "English"],
    need: "Need help?",
    find: "Find out more and contact us"
  }
};

export default function footer(state = initialState, action) {
  return state;
}
