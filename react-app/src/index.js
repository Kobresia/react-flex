import React from "react";
import { render } from "react-dom";
import { Provider } from "react-redux";
import { createStore, applyMiddleware } from "redux";

import thunk from "redux-thunk";

import App from "./App";
import ProductList from "./components/product-list/ProductList.js";
import ProductPage from "./components/product-page/ProductPage.js";
import "./index.css";
import { Router, Route, browserHistory, IndexRoute } from "react-router";
import { syncHistoryWithStore } from "react-router-redux";

import configureStore from "./configureStore";
import Header from "./components/header/Header";
import Footer from "./components/footer/Footer";
const store = configureStore();
const history = syncHistoryWithStore(browserHistory, store); //

render(
  <Provider store={store}>
    <div>
      <Router history={history}>
        <Route path="/" component={App}>
          <IndexRoute component={ProductList} />
          <Route path="/product-page" component={ProductPage} />
        </Route>
      </Router>
    </div>
  </Provider>,
  document.getElementById("root")
);
