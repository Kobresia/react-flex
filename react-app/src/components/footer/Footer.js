import React, { PropTypes, Component } from "react";
import { connect } from "react-redux";

import styled from "styled-components";
import { Link } from "react-router";

const Foot = styled.footer`
  background-color: #f3f3f3;
  margin-top: 4rem;
  padding: 2rem 0.5rem 2rem 0.5rem;
  .row {
    margin: 0 -0.5rem;
    width: 100%;
  }

  @media screen and (min-width: 62rem) {
    padding: 4rem 4.625rem 3rem 4.625rem;
  }
`;
const FooterMenu = styled.div`
  display: none;

  @media screen and (min-width: 48rem) {
    display: block;
  }
`;

const MenuHeader = styled.h3`
  font-family: "Raleway", sans-serif;
  margin-bottom: 1rem;
  font-size: 0.75rem;
  font-weight: bold;
  text-align: left;
  color: #171717;
  text-transform: uppercase;
`;

const HeaderLink = styled(Link)`
  font-family: "Raleway", sans-serif;
  margin-bottom: 0.75rem;
  font-size: 0.75rem;
  display: block;
  text-decoration: none;
  font-weight: 600;
  text-align: left;
  color: #999999;
`;
const FtBtn = styled.div`
  @media screen and (min-width: 48rem) {
    margin-top: 1.25rem;
  }
`;
const BtnCon = styled.button`
  display: block;
  border: none;
  background: none;
  font-family: "Raleway", sans-serif;
  font-size: 0.75em;
  line-height: 1.33;
  text-align: left;
  color: #999999;
  padding: 0px;
  margin-bottom: 1rem;
  cursor: pointer;
  span {
    color: black;
  }
  @media screen and (min-width: 48rem) {
    display: inline-block;
  }
`;

const BtnLan = styled.button`
  display: block;
  border: none;
  background: none;
  font-family: "Raleway", sans-serif;
  font-size: 0.75em;
  line-height: 1.33;
  text-align: left;
  color: #999999;
  padding: 0px;
  margin-bottom: 1.5rem;
  cursor: pointer;
  span {
    color: black;
  }
  @media screen and (min-width: 48rem) {
    margin-left: 1.5rem;
    display: inline-block;
  }
`;

const NeedHelp = styled.div`
  @media screen and (min-width: 48rem) {
    display: none;
  }
`;

const Help = styled.h4`
  font-family: "Raleway", sans-serif;
  font-size: 1rem;
  font-weight: bold;
  line-height: 1.19;
  text-align: center;
  color: #171717;
  margin-bottom: 1rem;
`;
const Contact = styled.a`
  font-family: "Lora", serif;
  font-size: 0.875rem;
  font-style: italic;
  line-height: 1.21;
  text-align: center;
  display: block;

  color: #171717;
  text-decoration: none;
`;

class Footer extends Component {
  render() {
    const footer = this.props.footer;

    return (
      <Foot>
        <FooterMenu>
          <div className="row">
            <div className="col-md-3">
              <MenuHeader>{footer.leftmenu[0]}</MenuHeader>
              <nav>
                {footer.leftmenu.map(function(m, index) {
                  if (index > 0)
                    return (
                      <HeaderLink key={index} to="/{m}">
                        {m}
                      </HeaderLink>
                    );
                })}
              </nav>
            </div>
            <div className="col-md-3">
              <MenuHeader>{footer.middlemenu[0]}</MenuHeader>
              <nav>
                {footer.middlemenu.map(function(m, index) {
                  if (index > 0)
                    return (
                      <HeaderLink key={index} to="/{m}">
                        {m}
                      </HeaderLink>
                    );
                })}
              </nav>
            </div>
            <div className="col-md-3">
              <MenuHeader>{footer.rightmenu[0]}</MenuHeader>
              <nav>
                {footer.rightmenu.map(function(m, index) {
                  if (index > 0)
                    return (
                      <HeaderLink key={index} to="/{m}">
                        {m}
                      </HeaderLink>
                    );
                })}
              </nav>
            </div>
          </div>
        </FooterMenu>
        <FtBtn>
          <BtnCon type="button">
            {footer.bottommenu.ship[0]}:{" "}
            <span>{footer.bottommenu.ship[1]}</span>
          </BtnCon>
          <BtnLan type="button">
            {footer.bottommenu.lang[0]}:{" "}
            <span>{footer.bottommenu.lang[1]}</span>
          </BtnLan>
        </FtBtn>
        <NeedHelp>
          <Help>{footer.bottommenu.need}</Help>
          <Contact>{footer.bottommenu.find}</Contact>
        </NeedHelp>
      </Foot>
    );
  }
}

Footer.propTypes = {
  //  Footer: PropTypes.object.isRequired,
};

const mapStateToProps = (state, ownProps) => {
  return {
    footer: state.footer
  };
};

export default connect(mapStateToProps)(Footer);
