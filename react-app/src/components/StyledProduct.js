import React, { Component, Proptypes } from "react";
import styled from "styled-components";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Link } from "react-router";
const RecItem = styled(Link)`
  display: block;
  margin-bottom: 2rem;
  cursor: pointer;
  text-decoration: none;
  @media screen and (min-width: 48rem) {
    margin-bottom: 0;
  }
`;
const RecImg = styled.img`width: 100%;`;
const RecStyle = styled.div`
  display: flex;
  justify-content: space-between;
`;
const RecStyleT = styled.p`
  font-family: "Raleway", sans-serif;
  font-size: 0.75rem;
  line-height: 1.33;
  text-align: left;
  color: #171717;
  margin: 1rem 0px 0.5rem 0;
`;
const RecTit = styled.h3`
  margin: 0px;
  margin-bottom: 0.3125rem;
  font-family: "Lora", serif;
  line-height: 1rem;
  text-align: left;
  font-weight: 600;
  font-size: 0.75rem;
  color: #171717;
`;

const Available = styled.p`
  font-family: "Raleway", sans-serif;
  font-size: 0.75rem;
  line-height: 1.33;
  text-align: left;
  color: #171717;
  margin: 0.5rem 0px 0.5rem 0;
`;

const AvailQuan = styled.span`border-bottom: 1px solid black;`;
const RecPr = styled.p`
  font-family: "Raleway", sans-serif;
  font-size: 0.75rem;
  line-height: 1.17;
  text-align: left;
  color: #999999;
  margin: 0;
`;

export default function(props) {
  return (
    <div className="row">
      {props.rec.map(function(m, index) {
        return (
          <div className="col-xs-6 col-md-3" key={index}>
            <RecItem to="/product-page">
              <RecImg src={m.src} alt={m.title} />
              <RecStyle>
                <RecStyleT>Relaxed Fit</RecStyleT>
                <img src="img/shape.svg" alt="like" />
              </RecStyle>
              <RecTit>{m.title}</RecTit>
              <Available>
                Available in <AvailQuan>3 colours</AvailQuan>
              </Available>
              <RecPr>{m.price}.</RecPr>
            </RecItem>
          </div>
        );
      })}
    </div>
  );
}
