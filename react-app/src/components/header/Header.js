import React, { PropTypes, Component } from "react";
import { connect } from "react-redux";
import { Link } from "react-router";
import styled from "styled-components";

const Wrapper = styled.div`
  display: flex;
  align-items: center;
  margin-bottom: 1.5rem;
  justify-content: center;
`;
const LogoLink = styled.a`
  display: block;
  cursor: pointer;
`;

const MenuOpen = styled.button`
  position: absolute;
  left: 0.5rem;
  width: 1rem;
  top: 1.32rem;
  height: 10px;
  border: none;
  background: url(./img/hamburger.svg) repeat-x;
  cursor: pointer;

  @media screen and (min-width: 48rem) {
    display: none;
  }
`;

const ChooseShop = styled.button`
  display: none;
  position: absolute;
  left: 0.5rem;
  border: none;
  background: none;
  font-size: 0.75rem;
  font-family: "Raleway", sans-serif;
  font-weight: 600;
  color: #999999;
`;
const NavMenu = styled.div`
  display: none;
  margin: 0 auto;
  flex-flow: row wrap;
  align-items: stretch;

  a {
    display: inline-block;
    padding: 1rem;
    text-decoration: none;
    border-bottom: 1px solid white;
  }

  a:hover {
    color: black;
    border-bottom: 1px solid black;
  }

  a:hover > span {
    color: black;
  }

  a span {
    text-transform: uppercase;
    font-size: 0.75rem;
    font-family: "Raleway", sans-serif;
    font-weight: 600;
    color: #999999;
    line-height: 1.33;
    letter-spacing: 1.5px;
  }

  @media screen and (min-width: 48rem) {
    display: flex;
  }
`;
const LinkImg = styled.img`
  @media screen and (min-width: 48rem) {
    height: 1rem;
  }
  @media (min-width: 62em) {
    margin-bottom: 2rem;
  }
`;
const Head = styled.header`
  padding: 1rem 0rem;
  display: flex;
  justify-content: center;
  align-items: center;
  position: relative;
  flex-flow: column;
  @media screen and (min-width: 48rem) {
    padding: 1.5rem 0rem 1rem 0;
  }
  @media (min-width: 62em) {
    padding: 2rem 0rem 0rem 0;
  }
`;
export default class Header extends Component {
  render() {
    const header = this.props.header;

    return (
      <Head>
        <Wrapper>
          <MenuOpen />
          <ChooseShop type="button">Shopping in: United Kingdom (£)</ChooseShop>
          <LogoLink>
            <LinkImg src="img/logo.svg" alt="logo" />
          </LogoLink>
        </Wrapper>
        <NavMenu>
          {header.menu.map(function(m, index) {
            return (
              <Link key={index} to="/">
                <span>{m}</span>
              </Link>
            );
          })}
        </NavMenu>
      </Head>
    );
  }
}

Header.propTypes = {
  header: PropTypes.object.isRequired
};
