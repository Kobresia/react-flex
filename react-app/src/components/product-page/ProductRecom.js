import React, { Component, Proptypes } from "react";
import styled from "styled-components";
import StyledProduct from "./../StyledProduct.js";
const Recommend = styled.div`
  margin-top: 1rem;

  @media screen and (min-width: 62rem) {
    margin: 4rem 4.625rem;
  }
`;
const WeRec = styled.h2`
  padding: 0px 0.5rem;
  font-family: "Raleway", sans-serif;
  font-size: 1rem;
  text-transform: uppercase;
  font-weight: 500;
  padding-bottom: 1rem;

  @media screen and (min-width: 62rem) {
    text-align: center;

    padding-bottom: 2rem;
  }
`;

const More = styled.div`
  padding: 0px 0.5rem;
  @media screen and (min-width: 48rem) {
    display: none;
  }
`;
const Foryou = styled.h2`
  font-family: "Raleway", sans-serif;
  font-size: 1em;
  font-weight: 500;
  line-height: 1.19;
  text-align: left;
  color: #171717;
`;

const MoreLink = styled.a`
  display: block;
  font-family: "Lora", serif;
  font-size: 0.875rem;
  font-style: italic;
  line-height: 1.21;
  text-align: left;
  color: #171717;
  margin-top: 1rem;
  text-decoration: none;
`;

export default class ProductRecom extends Component {
  render() {
    const rec = this.props.recommend.rec;

    return (
      <div>
        <Recommend>
          <WeRec> We recommend</WeRec>

          <StyledProduct rec={rec} />
        </Recommend>

        <More>
          <Foryou>MORE FOR YOU</Foryou>
          <MoreLink href="#">Men’s Black Trench Coats</MoreLink>
          <MoreLink href="#">Men’s Black Trench Coats</MoreLink>
          <MoreLink href="#">Men’s Black Trench Coats</MoreLink>
        </More>
      </div>
    );
  }
}
