import React, { PropTypes, Component } from "react";
import styled from "styled-components";
import { connect } from "react-redux";
const Desc = styled.div`
  border-top: solid 1px #c6c6c6;
  margin: 2rem auto;
  padding-top: 2rem;

  @media (min-width: 48em) {
    border-top: 0;
    margin: 0.5rem auto 0 0;
    padding-top: 0rem;
  }

  @media (min-width: 62em) {
    margin: 4rem 4.625rem;
  }
`;
const BtnDesc = styled.button`
  width: 100%;
  text-align: left;
  padding: 0px;
  margin-bottom: 2rem;
  border: none;
  cursor: pointer;
  background: none;
  @media (min-width: 48em) {
    margin-bottom: 1rem;
  }
`;

const H2Desc = styled.h2`
  font-family: "Raleway", sans-serif;
  font-size: 1rem;
  text-transform: uppercase;
  font-weight: 500;
  &::after {
    content: url("./img/triangle.svg");
    float: right;
  }
  @media (min-width: 48em) {
    &::after {
      content: "";
      float: initial;
    }
  }
`;

const DescText = styled.article`
  font-family: "Lora", serif;
  font-size: 0.875rem;
  line-height: 1.5rem;
  color: #171717;

  p {
    margin: 0;
  }
`;
const BigCoat = styled.img`
  display: none;

  @media (min-width: 62em) {
    width: 100%;
    display: block;
  }
`;

const MoreImg = styled.div`
  display: none;
  @media (min-width: 62em) {
    margin-top: 4rem;
    display: flex;
  }
`;

/*dangerouslySetInnerHTML={{__html: innerText}}*/
export default class ProductDesc extends Component {
  render() {
    const out = this.props.prodDesc.out;

    return (
      <Desc>
        <div className="row">
          <div className="col-xs-12 col-md-12 col-lg-6">
            <BtnDesc type="button">
              <H2Desc>Description</H2Desc>
            </BtnDesc>
            <DescText dangerouslySetInnerHTML={{ __html: out }} />
          </div>
          <div className="col-xs-12 col-md-12 col-lg-6 col-pic">
            <BigCoat src="img/bitmap@3x1.jpg" alt="Coat" />
          </div>
        </div>
        <MoreImg>
          <div className="row">
            <div className="col-lg-4">
              <BigCoat src="img/product-img-3.jpg" alt="Coat" />
            </div>
            <div className="col-lg-4 col-middle">
              <BigCoat src="img/product-img-2.jpg" alt="Coat" />
            </div>
            <div className="col-lg-4">
              <BigCoat src="img/product-img-3.jpg" alt="Coat" />
            </div>
          </div>
        </MoreImg>
      </Desc>
    );
  }
}
/*ProductDesc.propTypes = {
  prodDesc: PropTypes.object.isRequired
};*/
