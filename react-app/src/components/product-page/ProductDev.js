import React, { Component } from "react";
import styled from "styled-components";

const DevTexter = styled.div`
  border-top: solid 1px #c6c6c6;
  margin: 2rem auto;
  padding-top: 2rem;

  @media (min-width: 62em) {
    margin: 0rem 4.625rem;
    padding-top: 0;
  }
`;

const DevText = styled.div`
  border-top: solid 1px #c6c6c6;

  padding-top: 2rem;
  @media screen and (min-width: 48rem) {
    border: none;
    padding-top: 0px;
    .row {
      display: flex;
    }
  }
  .row {
    display: none;
  }
`;
const DelBtn = styled.button`
  width: 100%;
  text-align: left;
  padding: 0px;
  margin-bottom: 2rem;
  border: none;
  cursor: pointer;
  background: none;

  @media (min-width: 48em) {
    display: none;
  }
`;

const DelBtnH2 = styled.h2`
  font-family: "Raleway", sans-serif;
  margin: 0px 0.5rem;
  font-size: 1rem;
  text-transform: uppercase;
  font-weight: 500;
  &::after {
    content: url("./img/triangle.svg");
    float: right;
    transform: rotate(180deg);
  }
`;
const DevImg = styled.img`width: 100%;`;
const DelBlock = styled.div`@media (min-width: 62em) {padding-left: 6rem;}`;

const DelHeader = styled.h2`
  font-family: "Raleway", sans-serif;
  font-size: 1rem;
  text-transform: uppercase;
  font-weight: 500;
  padding-bottom: 1rem;
  color: #171717;
`;
const HeaderH3 = styled.h3`
  font-family: "Raleway", sans-serif;
  font-size: 0.75rem;
  font-weight: bold;
  text-align: left;
  color: #171717;
`;
const MesText = styled.p`
  font-family: "Raleway", sans-serif;
  font-size: 0.75rem;
  line-height: 1.33;
  text-align: left;
  color: #171717;
  margin: 0.25rem 0px 1.5rem 0px;
`;
export default class ProductDev extends Component {
  render() {
    return (
      <DevTexter>
        <DelBtn type="button">
          <DelBtnH2>Delivery</DelBtnH2>
        </DelBtn>
        <DevText>
          <div className="row">
            <div className="col-xs-7 col-lg-6">
              <DevImg src="img/bitmap@2xd.jpg" alt="Delivery" />
            </div>
            <DelBlock className="col-xs-5 col-lg-6">
              <DelHeader>Delivery</DelHeader>
              <div>
                <HeaderH3>Free Next Day Delivery</HeaderH3>
                <MesText>
                  Order before 7pm Monday to Thursday for delivery the next day
                </MesText>
              </div>
              <div>
                <HeaderH3>Collect-in-Store</HeaderH3>
                <MesText>
                  Order online today and pick up your items in store as early as
                  tomorrow
                </MesText>
              </div>
              <div>
                <HeaderH3>Free Returns</HeaderH3>
                <MesText>Enjoy free returns on your order</MesText>
              </div>
              <div>
                <HeaderH3>Free Gift Packaging</HeaderH3>
                <MesText>
                  Discover our gift packaging, a gold lined box tied with a
                  coloured ribbon
                </MesText>
              </div>
            </DelBlock>
          </div>
        </DevText>
      </DevTexter>
    );
  }
}
