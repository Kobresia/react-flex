import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import styled from "styled-components";
import ProductCard from "./ProductCard.js";
import ProductDesc from "./ProductDesc.js";
import ProductDev from "./ProductDev.js";
import ProductRecom from "./ProductRecom.js";

import * as actions from "../../actions/actions";

class ProductPage extends Component {
  render() {
    const { prodDesc, recommend, card } = this.props;
    const changeSize = this.props.actions.changeSize;

    return (
      <main>
        <ProductCard changeSize={changeSize} size={card} />
        <ProductDesc prodDesc={prodDesc} />
        <ProductDev />
        <ProductRecom recommend={recommend} />
      </main>
    );
  }
}

function mapStateToProps(state) {
  return {
    prodDesc: state.prodDesc,
    recommend: state.recommend,
    card: state.card.size
  };
}
// connect-подключи React компонент к Redux store.
function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(actions, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(ProductPage);
