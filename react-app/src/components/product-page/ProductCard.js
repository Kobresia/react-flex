import React, { Component } from "react";
import styled from "styled-components";

const Char = styled.div`
  @media (min-width: 62em) {
    background-color: #d4bdad;
    padding: 0 4.625rem;
  }
`;

const Title = styled.h1`
  font-family: "Lora", serif;
  font-size: 1.25rem;
  padding: 1rem 0.5rem;
  margin: 0px;
  font-weight: 400;
  @media (min-width: 62em) {
    display: none;
  }
`;

const Gallery = styled.div`
  width: 300px;
  height: 400px;
  display: flex;
  overflow: hidden;
  margin-bottom: 1rem;

  @media (min-width: 62em) {
    width: 488px;
    height: 651px;
    display: flex;
    overflow: hidden;
    margin-bottom: 0px;
  }
`;

const GalImage = styled.img`width: 100%;`;

const ProdInfo = styled.div`
  margin: 0rem 0rem 1rem;
  font-family: "Raleway", sans-serif;
  .row {
    width: 100%;
    margin: 0 -0.5rem;
  }
  @media (min-width: 62em) {
    margin: 9rem 0;
  }
`;
const TitleDesk = styled.h1`
  display: none;

  @media (min-width: 62em) {
    display: block;
    line-height: 1.17;
    color: #111;
    font-family: "Lora", serif;
  }
`;
const ProdCont = styled.div`
  display: flex;
  justify-content: space-between;
`;
const Price = styled.div`
  font-size: 1rem;
  font-weight: 200;

  @media (min-width: 62em) {
    font-family: "Raleway", sans-serif;
    margin-top: 0.5rem;
    font-weight: 500;
    text-align: left;
    color: #171717;
  }
`;
const Code = styled.div`
  display: none;

  @media (min-width: 62em) {
    font-size: 0.75rem;
    margin: 0;
  }
`;

const Color = styled.div`
  margin: 1rem 0px;
  font-size: 0.75rem;

  @media (min-width: 62em) {
    margin: 3rem 0px 1rem;
    display: flex;
    justify-content: space-between;
  }
`;

const Chooses = styled.span`font-weight: 700;`;

const ProductCol = styled.div`margin-bottom: 1rem;`;

const BtnBlack = styled.button`
  width: 40px;
  height: 40px;
  border-radius: 100px;
  cursor: pointer;
  background-color: #000000;
  border: solid 1px #000000;
`;
const BtnBrown = styled.button`
  width: 40px;
  height: 40px;
  border-radius: 100px;
  cursor: pointer;
  background-color: #cfa880;
  border: solid 1px #000000;
  margin-left: 1rem;
`;
const HR = styled.hr`
  margin: 2rem 0px;
  border: none;
  border-top: solid 1px #c6c6c6;
  @media (min-width: 62em) {
    display: none;
  }
`;
const Selector = styled.button`
  width: 100%;
  border-radius: 2px;
  p {
    font-family: "Raleway", sans-serif;
    font-size: 0.75rem;
    line-height: 1.33;
    padding: 1rem 0px;
    margin: 0;
    text-align: center;
    text-transform: uppercase;
    color: #ffffff;
  }
  @media (min-width: 62em) {
    margin-bottom: 0;
    margin-top: 1.5rem;
    margin-right: 0.5rem;
  }
  background-color: #171717;
  margin-bottom: 1rem;
  border: solid 1px #171717;
`;

const Size = styled.div`
  display: none;
  @media (min-width: 62em) {
    display: flex;
    justify-content: space-between;
  }
`;
const XL = styled.div`
  display: none;
  @media (min-width: 62em) {
    display: inline-block;
    margin-left: 0.5rem;
  }
`;

const BtnHelp = styled.button`
  display: none;
  @media (min-width: 62em) {
    font-family: "Raleway", sans-serif;
    font-size: 0.75rem;
    text-align: left;
    background: none;
    border: none;
    text-transform: uppercase;
    padding: 0px;
    display: inline-block;
    margin-bottom: 0rem;
  }
`;
const PrSizes = styled.div`
  display: none;
  @media (min-width: 62em) {
    display: flex;
    margin-bottom: 2.1rem;
    justify-content: space-between;
  }
`;
const SizesBtn = styled.button`
  border-radius: 2px;
  border: solid 1px #171717;
  background: none;
  padding: 0.5rem 1rem;
  margin-top: 1rem;
`;
const SizesBtnS = styled.button`
  border-radius: 2px;
  border: solid 1px #171717;
  background: none;
  padding: 0.5rem 1rem;
  margin-top: 1rem;
  @media (min-width: 62em) {
    margin-left: 0.5rem;
  }
`;

const InstoreFinder = styled.button`
  width: 100%;
  border-radius: 2px;

  border: solid 1px #171717;
  background-color: #fff;

  p {
    font-family: "Raleway", sans-serif;
    font-size: 0.75rem;
    line-height: 1.33;
    padding: 1rem 0px;
    margin: 0;
    text-align: center;
    text-transform: uppercase;
    color: #ffffff;
    color: #171717;
  }

  @media (min-width: 62em) {
    margin-left: 0.5rem;
    width: auto;

    background-color: #d4bdad;
  }
`;
const FreeDev = styled.div`
  display: none;
  margin-top: 1.5rem;

  @media (min-width: 62em) {
    display: block;
  }
`;
const DevTitle = styled.h3`
  font-family: "Raleway", sans-serif;
  font-size: 0.75rem;
  font-weight: bold;
  text-align: left;
  color: #171717;
`;
const DevOrder = styled.p`
  margin: 0.25rem 0 0 0;
  font-family: "Raleway", sans-serif;
  font-size: 0.75rem;
  line-height: 1.33;
  text-align: left;
  color: #171717;
`;
const HelpBtn = styled.button`
  font-family: "Raleway", sans-serif;
  font-size: 0.75rem;
  line-height: 1.33;
  text-align: left;
  margin-top: 1rem;
  background: none;
  border: none;
  text-transform: uppercase;
  padding: 0px;
  margin-bottom: 0rem;
  @media (min-width: 62em) {
    display: none;
  }
`;

export default class ProductCard extends Component {
  render() {
    const size = this.props.size;

    return (
      <Char>
        <Title>Long Cotton Gabardine Car Coat</Title>
        <div className="row">
          <div className="col-xs-12 col-md-7 col-lg-6">
            <Gallery>
              <GalImage
                src="img/bitmap@2x.jpg"
                alt="Long Cotton Gabardine Car Coat"
              />
              <GalImage
                src="img/bitmap.jpg"
                alt="Long Cotton Gabardine Car Coat"
              />
              <GalImage
                src="img/bitmap.jpg"
                alt="Long Cotton Gabardine Car Coat"
              />
              <GalImage
                src="img/bitmap.jpg"
                alt="Long Cotton Gabardine Car Coat"
              />
            </Gallery>
          </div>
          <div className="col-xs-12 col-md-5 col-lg-6">
            <ProdInfo>
              <TitleDesk>Long Cotton Gabardine Car Coat</TitleDesk>
              <ProdCont>
                <Price>110 000 руб</Price>
                <Code>Item 3993223</Code>
              </ProdCont>
              <Color>
                <div className="row">
                  <div className="col-xs-12 col-md-12 col-lg-6 col-left">
                    <ProductCol>
                      <span>Colour:</span>
                      <Chooses>Honey</Chooses>
                    </ProductCol>
                    <BtnBlack type="button" />
                    <BtnBrown type="button" />
                    <HR />
                    <Selector type="button">
                      <p>Select a size</p>
                    </Selector>
                  </div>
                  <div className="col-xs-12 col-md-12 col-lg-6 col-right">
                    <div>
                      <Size>
                        <XL>
                          <span>Size:</span>
                          <Chooses>{size}</Chooses>
                        </XL>
                        <BtnHelp type="button">Need size help?</BtnHelp>
                      </Size>
                      <PrSizes>
                        <SizesBtnS
                          type="button"
                          onClick={this.props.changeSize.bind(this, "S")}
                        >
                          {" "}
                          S
                        </SizesBtnS>
                        <SizesBtn
                          type="button"
                          onClick={this.props.changeSize.bind(this, "M")}
                        >
                          {" "}
                          M
                        </SizesBtn>
                        <SizesBtn
                          type="button"
                          onClick={this.props.changeSize.bind(this, "L")}
                        >
                          {" "}
                          L
                        </SizesBtn>
                        <SizesBtn
                          type="button"
                          onClick={this.props.changeSize.bind(this, "XL")}
                        >
                          {" "}
                          XL
                        </SizesBtn>
                      </PrSizes>
                    </div>
                    <InstoreFinder type="button">
                      <p>Find in store</p>
                    </InstoreFinder>
                  </div>
                </div>
              </Color>
              <FreeDev>
                <DevTitle>Free Next Day Delivery</DevTitle>
                <DevOrder>
                  Order before 7pm Monday to Thursday for delivery the next day
                </DevOrder>
              </FreeDev>
              <HelpBtn>Need size help?</HelpBtn>
            </ProdInfo>
          </div>
        </div>
      </Char>
    );
  }
}
