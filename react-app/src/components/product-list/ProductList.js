import { bindActionCreators } from "redux";
import React, { Component, Proptypes } from "react";
import { connect } from "react-redux";
import * as actions from "../../actions/actions";
import styled from "styled-components";
import StyledProduct from "./../StyledProduct.js";
const List = styled.main`
  margin-top: 2rem;
  margin-bottom: 5rem;
  .row {
    margin: 0;
  }
  @media (min-width: 62em) {
    margin-left: 4rem;
    margin-right: 4rem;
  }
`;
const Desc = styled.div`
  background-color: #f3f3f3;
  padding: 2rem 0.5rem;
  @media (min-width: 62em) {
    padding-left: 4rem;
    padding-right: 4rem;
  }
`;

const DescH1 = styled.h1`
  font-family: "Lora", serif;
  font-size: 1, 625rem;
  text-align: left;
  margin-bottom: 1rem;
  color: #171717;
`;
const CatDesc = styled.div`
  opacity: 0.87;
  font-family: "Raleway", sans-serif;
  font-size: 0.75rem;
  line-height: 1.67;
  text-align: left;
  color: #171717;
`;
const Sort = styled.div`margin-top: 1.5rem;`;
const BtnSort = styled.button`
  border: none;
  background: none;
  padding: 0;
  font-family: "Raleway", sans-serif;
  font-size: 0.75rem;
  line-height: 1.33;
  text-align: left;
  color: #171717;
  & > div::after {
    content: url("../img/shape(1).svg");
    float: right;
    margin-left: 0.5rem;
    margin-right: 1rem;
  }
`;

const Type = styled.h2`
  font-family: "Lora", sans-serif;
  font-size: 1rem;
  text-align: left;
  color: #171717;
  margin: 0 0.5rem;
  margin-bottom: 1rem;
  margin-top: 2rem;
`;

const Show = styled.div`
  font-family: "Lora", sans-serif;
  font-size: 1rem;
  text-align: center;
  display: block;
  color: #171717;
  margin-bottom: 1rem;
  width: 100%;
  margin-top: 3rem;
`;
const MoreP = styled.button`
  display: block;
  margin: 0 auto;
  margin-top: 2rem;
  padding: 1rem 2rem;
  background: none;
  border: solid 1px #171717;
`;
const Under = styled.hr`
  margin: 2rem 0px;
  border: none;
  border-top: solid 1px #c6c6c6;
  margin: 1rem 0px 3rem;
  @media (min-width: 62em) {
    display: none;
  }
`;

class ProductList extends Component {
  render() {
    const cat1 = this.props.rec.cat1;
    const cat2 = this.props.rec.cat2;
    const cat1Tit = this.props.rec.cat1Tit;
    const cat2Tit = this.props.rec.cat2Tit;

    return (
      <div>
        <Desc>
          <DescH1>Men’s Clothing</DescH1>
          <CatDesc>
            Explore our menswear collection for the season. Sculptural knitwear,
            sweatshirts, artist overalls and oversized cabans feature alongside
            our signature trench coat in both heritage.
          </CatDesc>
          <Sort>
            <BtnSort type="button">
              <div>Category</div>
            </BtnSort>
            <BtnSort type="button">
              <div>Colour</div>
            </BtnSort>
            <BtnSort type="button">
              <div>Size</div>
            </BtnSort>
            <BtnSort type="button">
              <div>Sort by price</div>
            </BtnSort>
          </Sort>
        </Desc>
        <List>
          <Type>{cat1Tit}</Type>
          <StyledProduct rec={cat1} />
          <Under />

          <Type>{cat2Tit}</Type>

          <StyledProduct rec={cat2} />
          <Show>Showing 8 of 17</Show>
          <MoreP type="button">View 9 more</MoreP>
        </List>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    rec: state.prodlist
  };
}
// connect-подключи React компонент к Redux store.
function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(actions, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(ProductList);
