import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import ProductPage from "./components/product-page/ProductPage";
import ProductList from "./components/product-list/ProductList";
import Header from "./components/header/Header";
import Footer from "./components/footer/Footer";

import * as actions from "./actions/actions";
class App extends Component {
  render() {
    const { header } = this.props;
    return (
      <div>
        <Header header={header} />
        {this.props.children}
        <Footer />
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    header: state.header
  };
}
// connect-подключи React компонент к Redux store.
function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(actions, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
